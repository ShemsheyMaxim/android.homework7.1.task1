package com.maxim.homework71.sorted;

import com.maxim.homework71.model.InformationAboutCountry;


import java.util.Comparator;

/**
 * Created by Максим on 31.05.2017.
 */

public class SortedByRegion implements Comparator<String> {

    @Override
    public int compare(String region1, String region2) {

            return region1.compareTo(region2);
        }

}

