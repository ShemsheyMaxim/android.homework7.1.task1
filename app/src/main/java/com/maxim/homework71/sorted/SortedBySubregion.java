package com.maxim.homework71.sorted;

import com.maxim.homework71.model.InformationAboutCountry;

import java.util.Comparator;

/**
 * Created by Максим on 04.06.2017.
 */

public class SortedBySubregion implements Comparator<String> {
    @Override
    public int compare(String subregion1, String subregion2) {

        return subregion1.compareTo(subregion2);
    }
}
