package com.maxim.homework71.sorted;

import com.maxim.homework71.model.InformationAboutCountry;

import java.util.Comparator;

/**
 * Created by Максим on 20.06.2017.
 */

public class SortedByCountriesName implements Comparator<InformationAboutCountry> {
    @Override
    public int compare(InformationAboutCountry obj1, InformationAboutCountry obj2) {
        String str1 = obj1.getName();
        String str2 = obj2.getName();

        return str1.compareTo(str2);
    }
}
