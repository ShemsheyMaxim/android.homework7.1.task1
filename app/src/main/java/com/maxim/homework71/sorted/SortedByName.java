package com.maxim.homework71.sorted;

import com.maxim.homework71.model.InformationAboutCountry;

import java.util.Comparator;

/**
 * Created by Максим on 04.06.2017.
 */

public class SortedByName implements Comparator<String> {
    @Override
    public int compare(String name1, String name2) {
        return name1.compareTo(name2);
    }
}
