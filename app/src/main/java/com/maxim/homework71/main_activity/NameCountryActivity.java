package com.maxim.homework71.main_activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.maxim.homework71.adapter.NameCountryAdapter;
import com.maxim.homework71.adapter.SubregionAdapter;
import com.maxim.homework71.model.InformationAboutCountry;
import com.maxim.homework71.R;
import com.maxim.homework71.Retrofit;
import com.maxim.homework71.sorted.SortedByName;
import com.maxim.homework71.sorted.SortedByRegion;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class NameCountryActivity extends AppCompatActivity implements NameCountryAdapter.NameCountryClickListener {
    public static final String FILE_NAME = "my_file";
    public static final String KEY = "nameCountry";

    private String subregionName;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_country);

        recyclerView = (RecyclerView) findViewById(R.id.nca_recycler_view);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);

        subregionName = getIntent().getStringExtra(SubregionActivity.KEY);

        readData();
    }

    private void readData() {
        StringBuilder json = new StringBuilder();
        try {
            FileInputStream fis = openFileInput(FILE_NAME);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            String line;
            while ((line = reader.readLine()) != null) {
                json.append(line);
            }
            fis.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        Type listType = new TypeToken<ArrayList<InformationAboutCountry>>() {
        }.getType();
        List<InformationAboutCountry> informationAboutCoutries = new Gson().fromJson(json.toString(), (java.lang.reflect.Type) listType);
        updateUI(subregionName, informationAboutCoutries);
    }

    private void updateUI(String subregionName, List<InformationAboutCountry> nameCountries) {
        Set<String> nameCountriesSet = new HashSet<>();
        for (InformationAboutCountry nameCountry : nameCountries) {
            if (nameCountry.getSubregion().equals(subregionName)) {
                nameCountriesSet.add(nameCountry.getName());
            }
        }
        List<String> nameCountriesList = new ArrayList<>();
        for (final String nameCountry : nameCountriesSet) {
            nameCountriesList.add(nameCountry);
        }
        Collections.sort(nameCountriesList, new SortedByName());
        NameCountryAdapter nameCountryAdapter = new NameCountryAdapter(NameCountryActivity.this, nameCountriesList, NameCountryActivity.this);
        recyclerView.setAdapter(nameCountryAdapter);
        nameCountryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(String nameCountry) {
        Intent intent = new Intent(this, InformationAboutCountryActivity.class);
        intent.putExtra(KEY, nameCountry);
        startActivity(intent);
    }
}
