package com.maxim.homework71.main_activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.maxim.homework71.R;
import com.maxim.homework71.adapter.FilterNameCountryAdapter;
import com.maxim.homework71.adapter.FilterNameCountryAdapter.FilterNameCountryClickListener;
import com.maxim.homework71.adapter.NameCountryAdapter;
import com.maxim.homework71.model.InformationAboutCountry;
import com.maxim.homework71.sorted.SortedByCountriesName;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;
import static java.security.AccessController.getContext;

/**
 * Created by Максим on 20.06.2017.
 */

public class ListAllCountriesActivity extends AppCompatActivity implements FilterNameCountryAdapter.FilterNameCountryClickListener {

    public static final String FILE_NAME = "my_file";
    public static final String KEY = "nameCountry";

    private FilterNameCountryAdapter filterNameCountryAdapter;

    private RecyclerView recyclerView;
    private EditText search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        recyclerView = (RecyclerView) findViewById(R.id.nca_recycler_view);
        search = (EditText) findViewById(R.id.editText_search);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);

        readData();
    }

    private void readData() {
        StringBuilder json = new StringBuilder();
        try {
            FileInputStream fis = openFileInput(FILE_NAME);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            String line;
            while ((line = reader.readLine()) != null) {
                json.append(line);
            }
            fis.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        Type listType = new TypeToken<ArrayList<InformationAboutCountry>>() {
        }.getType();
        List<InformationAboutCountry> informationAboutCoutries = new Gson().fromJson(json.toString(), (java.lang.reflect.Type) listType);
        updateUI(informationAboutCoutries);
    }

    private void updateUI(List<InformationAboutCountry> nameCountries) {

        Collections.sort(nameCountries, new SortedByCountriesName());

        filterNameCountryAdapter = new FilterNameCountryAdapter(ListAllCountriesActivity.this, nameCountries,ListAllCountriesActivity.this);
        recyclerView.setAdapter(filterNameCountryAdapter);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                ListAllCountriesActivity.this.filterNameCountryAdapter.getFilter().filter(cs);
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
        filterNameCountryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(InformationAboutCountry informationAboutCountry) {
        Intent intent = new Intent(this, InformationAboutCountryActivity.class);
        intent.putExtra(KEY, informationAboutCountry.getName());
        startActivity(intent);
    }
}
