package com.maxim.homework71.main_activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.maxim.homework71.adapter.NameCountryAdapter;
import com.maxim.homework71.model.InformationAboutCountry;
import com.maxim.homework71.model.Language;
import com.maxim.homework71.R;
import com.maxim.homework71.Retrofit;
import com.maxim.homework71.sorted.SortedByRegion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class InformationAboutCountryActivity extends AppCompatActivity {
    public static final String DIR_NAME = "assets_flags";
    public static final String FILE_NAME = "my_file";
    public static final String folderPathWithFlags = "/sdcard/Flags/flags/";
    public static final String imageExtension = ".png";

    private String nameCountry;
    private InformationAboutCountry informationAboutCountry;
    private TextView regionTextView;
    private TextView subregionTextView;
    private TextView nameTextView;
    private TextView capitalTextView;
    private TextView populationTextView;
    private TextView languageTextView;
    private TextView bordersTextView;
    private TextView areaTextView;
    private ImageView flagImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_about_country);

        regionTextView = (TextView) findViewById(R.id.iaca_textView_region);
        subregionTextView = (TextView) findViewById(R.id.iaca_textView_subregion);
        nameTextView = (TextView) findViewById(R.id.iaca_textView_name);
        capitalTextView = (TextView) findViewById(R.id.iaca_textView_capital);
        populationTextView = (TextView) findViewById(R.id.iaca_textView_population);
        languageTextView = (TextView) findViewById(R.id.iaca_textView_languages);
        bordersTextView = (TextView) findViewById(R.id.iaca_textView_borders);
        areaTextView = (TextView) findViewById(R.id.iaca_textView_area);
        flagImageView = (ImageView) findViewById(R.id.imageView_flag);

        nameCountry = getIntent().getStringExtra(NameCountryActivity.KEY);
        readData();
    }

    private void readData() {
        StringBuilder json = new StringBuilder();
        try {
            FileInputStream fis = openFileInput(FILE_NAME);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            String line;
            while ((line = reader.readLine()) != null) {
                json.append(line);
            }
            fis.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        Type listType = new TypeToken<ArrayList<InformationAboutCountry>>() {
        }.getType();
        List<InformationAboutCountry> informationAboutCoutries = new Gson().fromJson(json.toString(), (java.lang.reflect.Type) listType);
        updateUI(nameCountry, informationAboutCoutries);
    }

    private void updateUI(String nameCountry, List<InformationAboutCountry> informationAboutCountries) {
        Set<String> informationAboutCountrySet = new HashSet<>();
        for (InformationAboutCountry informationAboutCountry : informationAboutCountries) {
            if (informationAboutCountry.getName().equals(nameCountry)) {
                informationAboutCountrySet.add(informationAboutCountry.getRegion());
                regionTextView.setText(informationAboutCountry.getRegion());
                subregionTextView.setText(informationAboutCountry.getSubregion());
                nameTextView.setText(informationAboutCountry.getName());
                capitalTextView.setText(informationAboutCountry.getCapital());
                populationTextView.setText(String.valueOf(informationAboutCountry.getPopulation()));

                Language language = informationAboutCountry.getLanguages().get(0);
                String strLanguage = language.getIso639_1() + ", " + language.getIso639_2()
                        + ", " + language.getName() + ", " + language.getNativeName();
                languageTextView.setText(strLanguage);

                String strBorders = "";
                for (int i = informationAboutCountry.getBorders().length - 1; i >= 0; i--) {
                    strBorders = strBorders + informationAboutCountry.getBorders()[i] + " ";
                }
                bordersTextView.setText(strBorders);

                areaTextView.setText(String.valueOf(informationAboutCountry.getArea()));
                loadIfExists();
            }
        }
        List<String> informationAboutCountryList = new ArrayList<>();
        for (final String informationAboutCountry : informationAboutCountrySet) {
            informationAboutCountryList.add(informationAboutCountry);
        }
        Collections.sort(informationAboutCountryList, new SortedByRegion());
    }

    private void loadIfExists() {
        File directory = new File(Environment.getExternalStorageDirectory() + "/Flags");
        if (directory.exists()) {
            //read file
            try {
                String pathName = folderPathWithFlags + nameCountry.toLowerCase() + imageExtension;
                Bitmap bmp = BitmapFactory.decodeFile(pathName);
                flagImageView.setImageBitmap(bmp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
