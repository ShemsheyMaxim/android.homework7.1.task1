package com.maxim.homework71.main_activity;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.maxim.homework71.R;
import com.maxim.homework71.adapter.RegionAdapter;
import com.maxim.homework71.model.InformationAboutCountry;
import com.maxim.homework71.Retrofit;
import com.maxim.homework71.sorted.SortedByRegion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RegionActivity extends AppCompatActivity implements RegionAdapter.RegionClickListener {
    public static final String DIR_NAME = "assets_flags";
    public static final String FILE_NAME = "my_file";
    public static final String KEY = "region";

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_region);

        recyclerView = (RecyclerView) findViewById(R.id.ra_recycler_view);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);

        if (fileExistence(FILE_NAME)) {
            readData();
        } else {
            getData();
        }
        File directory = new File(Environment.getExternalStorageDirectory() + "/Flags");
        if(!directory.exists()) {
            unzipFromAssets(this, "flags.zip", "/sdcard/Flags/");
        }

        FloatingActionButton myFAB = (FloatingActionButton) findViewById(R.id.fab);
        myFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegionActivity.this, ListAllCountriesActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getData() {
        String name = "";
        Retrofit.getInformation(name, new Callback<List<InformationAboutCountry>>() {

            @Override
            public void success(List<InformationAboutCountry> informationAboutCountries, Response response) {
                updateUI(informationAboutCountries);
                saveData(informationAboutCountries);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "Exception with HTTP!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void saveData(List<InformationAboutCountry> informationAboutCountries) {
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            String json = new Gson().toJson(informationAboutCountries);
            fos.write(json.getBytes());
            fos.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    private void readData() {
        StringBuilder json = new StringBuilder();
        try {
            FileInputStream fis = openFileInput(FILE_NAME);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            String line;
            while ((line = reader.readLine()) != null) {
                json.append(line);
            }
            fis.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        Type listType = new TypeToken<ArrayList<InformationAboutCountry>>() {
        }.getType();
        List<InformationAboutCountry> informationAboutCoutries = new Gson().fromJson(json.toString(), (java.lang.reflect.Type) listType);
        updateUI(informationAboutCoutries);
    }

    private void updateUI(List<InformationAboutCountry> regions) {
        Set<String> regionsSet = new HashSet<>();
        for (InformationAboutCountry region : regions) {
            regionsSet.add(region.getRegion());
        }
        List<String> regionList = new ArrayList<>();
        for (final String region : regionsSet) {
            regionList.add(region);
        }
        Collections.sort(regionList, new SortedByRegion());
        RegionAdapter regionAdapter = new RegionAdapter(RegionActivity.this, regionList, RegionActivity.this);
        recyclerView.setAdapter(regionAdapter);
        regionAdapter.notifyDataSetChanged();
    }

    private boolean fileExistence(String fName) {
        File file = getBaseContext().getFileStreamPath(fName);
        return file.exists();
    }

    @Override
    public void onClick(String region) {
        Intent intent = new Intent(this, SubregionActivity.class);
        intent.putExtra(KEY, region);
        startActivity(intent);
    }

//    создать класс сингелтон в котором передавать обект страну.
//    private Map<String, HashMap<String, List<InformationAboutCountry>>> createDataHashMap(List<InformationAboutCountry> countries){
//        final Map<String, HashMap<String, List<InformationAboutCountry>>> result = new HashMap();
//        for (final InformationAboutCountry country : countries) {
//            final String regionName = country.getRegion();
//        }
//        return result;
//    }

    private static final int BUFFER_SIZE = 1024 * 10;
    private static final String TAG = "test";

    public void unzipFromAssets(Context context, String zipFile, String destination) {
        try {

            if (destination == null || destination.length() == 0) {
                destination = context.getFilesDir().getAbsolutePath();
            }
            AssetManager assetManager = getResources().getAssets();
            InputStream stream = assetManager.open(zipFile);
            unzip(stream, destination);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void unzip(InputStream stream, String destination) {
        dirChecker(destination, "");
        byte[] buffer = new byte[BUFFER_SIZE];
        try {
            ZipInputStream zis = new ZipInputStream(stream);
            ZipEntry ze = null;

            while ((ze = zis.getNextEntry()) != null) {
                Log.v(TAG, "Unzipping " + ze.getName());

                if (ze.isDirectory()) {
                    dirChecker(destination, ze.getName());
                } else {
                    File f = new File(destination + ze.getName());
                    if (!f.exists()) {
                        FileOutputStream fout = new FileOutputStream(destination + ze.getName());
                        int count;
                        while ((count = zis.read(buffer)) != -1) {
                            fout.write(buffer, 0, count);
                        }
                        zis.closeEntry();
                        fout.close();
                    }
                }

            }
            zis.close();
        } catch (Exception e) {
            Log.e(TAG, "unzip", e);
        }

    }

    private void dirChecker(String destination, String dir) {
        File f = new File(destination + dir);

        if (!f.isDirectory()) {
            boolean success = f.mkdirs();
            if (!success) {
                Log.w(TAG, "Failed to create folder " + f.getName());
            }
        }
    }
}

