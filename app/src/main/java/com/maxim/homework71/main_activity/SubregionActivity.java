package com.maxim.homework71.main_activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.maxim.homework71.R;
import com.maxim.homework71.Retrofit;

import com.maxim.homework71.adapter.RegionAdapter;
import com.maxim.homework71.adapter.SubregionAdapter;
import com.maxim.homework71.model.InformationAboutCountry;
import com.maxim.homework71.sorted.SortedByRegion;
import com.maxim.homework71.sorted.SortedBySubregion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SubregionActivity extends AppCompatActivity implements SubregionAdapter.SubregionClickListener {
    public static final String FILE_NAME = "my_file";
    public static final String KEY = "subregion";
    private String regionName;

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subregion);

        recyclerView = (RecyclerView) findViewById(R.id.sa_recycler_view);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);

        regionName = getIntent().getStringExtra(RegionActivity.KEY);
        readData();
    }

    private void readData() {
        StringBuilder json = new StringBuilder();
        try {
            FileInputStream fis = openFileInput(FILE_NAME);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            String line;
            while ((line = reader.readLine()) != null) {
                json.append(line);
            }
            fis.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        Type listType = new TypeToken<ArrayList<InformationAboutCountry>>() {}.getType();
        List<InformationAboutCountry> informationAboutCoutries = new Gson().fromJson(json.toString(), (java.lang.reflect.Type) listType);
        updateUI(regionName,informationAboutCoutries);
    }

    private void updateUI(String regionName, List<InformationAboutCountry> subregions) {
        Set<String> subregionsSet = new HashSet<>();
        for (InformationAboutCountry subregion : subregions) {
            if (subregion.getRegion().equals(regionName)) {
                subregionsSet.add(subregion.getSubregion());
            }
        }
        List<String> subregionList = new ArrayList<>();
        for (final String subregion : subregionsSet) {
            subregionList.add(subregion);
        }
        Collections.sort(subregionList, new SortedByRegion());
        SubregionAdapter subregionAdapter = new SubregionAdapter(SubregionActivity.this, subregionList, SubregionActivity.this);
        recyclerView.setAdapter(subregionAdapter);
        subregionAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(String subregion) {
        Intent intent = new Intent(this, NameCountryActivity.class);
        intent.putExtra(KEY, subregion);
        startActivity(intent);
    }
}
