package com.maxim.homework71.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.maxim.homework71.R;
import com.maxim.homework71.model.InformationAboutCountry;

import java.util.List;

/**
 * Created by Максим on 31.05.2017.
 */

public class SubregionAdapter extends RecyclerView.Adapter<SubregionAdapter.ViewHolder> {


        public interface SubregionClickListener {
            void onClick(String subregion);
        }

        interface ItemClickListener {
            void onClick(int position);
        }

        private Context context;
        private List<String> subregionList;
        private SubregionAdapter.SubregionClickListener subregionClickListener;
        private SubregionAdapter.ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClick(int position) {
                subregionClickListener.onClick(subregionList.get(position));
            }
        };

        public SubregionAdapter(Context context, List<String> subregionList, SubregionClickListener subregionClickListener) {
            this.context = context;
            this.subregionList = subregionList;
            this.subregionClickListener = subregionClickListener;
        }

        @Override
        public SubregionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View item = LayoutInflater.from(context).inflate(R.layout.item_view, parent, false);
            return new ViewHolder(item);
        }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String subregion = subregionList.get(position);
        holder.tvSubregion.setText(subregion);
    }

        @Override
        public int getItemCount() {
            return subregionList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private TextView tvSubregion;

            public ViewHolder(View item) {
                super(item);
                tvSubregion = (TextView) item.findViewById(R.id.textView_region);
                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemClickListener.onClick(getAdapterPosition());
                    }
                });
            }
        }
    }
