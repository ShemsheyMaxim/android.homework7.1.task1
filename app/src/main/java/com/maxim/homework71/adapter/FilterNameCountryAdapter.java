package com.maxim.homework71.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.maxim.homework71.R;
import com.maxim.homework71.main_activity.ListAllCountriesActivity;
import com.maxim.homework71.model.InformationAboutCountry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Максим on 20.06.2017.
 */

public class FilterNameCountryAdapter extends RecyclerView.Adapter<FilterNameCountryAdapter.ViewHolder> implements Filterable {

    public interface FilterNameCountryClickListener {
        void onClick(InformationAboutCountry informationAboutCountry);
    }

    interface ItemClickListener {
        void onClick(int position);
    }

    private Context context;
    private List<InformationAboutCountry> nameCountryList;
    private List<InformationAboutCountry> filteredList;
    private FilterNameCountryClickListener filterNameCountryClickListener;
    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onClick(int position) {
            filterNameCountryClickListener.onClick(filteredList.get(position));
        }
    };

    public FilterNameCountryAdapter(Context context, List<InformationAboutCountry> nameCountryList, FilterNameCountryClickListener filterNameCountryClickListener) {
        this.context = context;
        this.nameCountryList = nameCountryList;
        this.filteredList = nameCountryList;
        this.filterNameCountryClickListener = filterNameCountryClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.item_view, parent, false);
        return new ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(FilterNameCountryAdapter.ViewHolder holder, int position) {
        InformationAboutCountry nameCountry = filteredList.get(position);
        holder.tvNameCountry.setText(nameCountry.getName());
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvNameCountry;

        public ViewHolder(View item) {
            super(item);
            tvNameCountry = (TextView) item.findViewById(R.id.textView_region);
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onClick(getAdapterPosition());
                }
            });
        }
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<InformationAboutCountry> filterCountryByName = new ArrayList<>();
                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < nameCountryList.size(); i++) {
                    InformationAboutCountry nameCountries = nameCountryList.get(i);
                    if (nameCountries.getName().toLowerCase().startsWith(constraint.toString())||
                            nameCountries.getCapital().toLowerCase().startsWith(constraint.toString())) {
                        filterCountryByName.add(nameCountries);
                    }
                }
                results.count = filterCountryByName.size();
                results.values = filterCountryByName;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredList = (List<InformationAboutCountry>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }
}
