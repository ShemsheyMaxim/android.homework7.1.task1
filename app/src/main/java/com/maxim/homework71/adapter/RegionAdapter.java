package com.maxim.homework71.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.maxim.homework71.R;
import com.maxim.homework71.model.InformationAboutCountry;

import java.util.List;

/**
 * Created by Максим on 29.05.2017.
 */

public class RegionAdapter extends RecyclerView.Adapter<RegionAdapter.ViewHolder> {

    public interface RegionClickListener {
        void onClick(String region);
    }

    interface ItemClickListener {
        void onClick(int position);
    }

    private Context context;
    private List<String> regionList;
    private RegionClickListener regionClickListener;
    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onClick(int position) {
            regionClickListener.onClick(regionList.get(position));
        }
    };

    public RegionAdapter(Context context, List<String> regionList, RegionClickListener regionClickListener) {
        this.context = context;
        this.regionList = regionList;
        this.regionClickListener = regionClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.item_view, parent, false);
        return new ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String region = regionList.get(position);
        holder.tvRegion.setText(region);
    }

    @Override
    public int getItemCount() {
        return regionList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvRegion;

        public ViewHolder(View item) {
            super(item);
            tvRegion = (TextView) item.findViewById(R.id.textView_region);
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onClick(getAdapterPosition());
                }
            });
        }
    }
}
