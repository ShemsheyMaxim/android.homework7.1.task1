package com.maxim.homework71.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;


import com.maxim.homework71.R;
import com.maxim.homework71.model.InformationAboutCountry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Максим on 31.05.2017.
 */

public class NameCountryAdapter extends RecyclerView.Adapter<NameCountryAdapter.ViewHolder> {

    public interface NameCountryClickListener {
        void onClick(String nameCountry);
    }
    interface ItemClickListener {
        void onClick(int position);
    }

    private Context context;
    private List<String> nameCountryList;
    private NameCountryClickListener nameCountryClickListener;
    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onClick(int position) {
            nameCountryClickListener.onClick(nameCountryList.get(position));
        }
    };

    public NameCountryAdapter(Context context, List<String> nameCountryList, NameCountryClickListener nameCountryClickListener) {
        this.context = context;
        this.nameCountryList = nameCountryList;
        this.nameCountryClickListener = nameCountryClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.item_view, parent, false);
        return new ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(NameCountryAdapter.ViewHolder holder, int position) {
        String nameCountry = nameCountryList.get(position);
        holder.tvNameCountry.setText(nameCountry);
    }

    @Override
    public int getItemCount() {
        return nameCountryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tvNameCountry;

        public ViewHolder(View item) {
            super(item);
            tvNameCountry = (TextView) item.findViewById(R.id.textView_region);
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onClick(getAdapterPosition());
                }
            });
        }
    }
}
